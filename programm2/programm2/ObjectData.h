
#ifndef OBJECT_DATA_H
#define OBJECT_DATA_H

#include "FlatData.h"
#include <string>

using namespace std;

class CObjectData
{
   public:
      CObjectData(CFlatData* pFlat, int ObjType, string ObjName,int x,int y);
      ~CObjectData();
      CFlatData* m_pFlat;
      int m_ObjType;
      string m_ObjName;
      int m_ObjCoordX;
      int m_ObjCoordY;
};

#endif