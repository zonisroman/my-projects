
#ifndef MOVING_OBJECT_H
#define MOVING_OBJECT_H

#include <string>

using namespace std;

class CFlat;

class CMovingObject
{
   public:
      CMovingObject(int x, int y, string ObjectName, int ObjectType, CFlat* pFlat);
      virtual ~CMovingObject();
      virtual void moveX(int ShiftX);
      virtual void moveY(int ShiftY);
      virtual void setFlat(CFlat* pFlat);
      virtual string getName();
      int getCoordX();
      int getCoordY();
      int getType();
      CFlat* getFlat();
   protected:
      string m_ObjectName;
      int m_CurrentX; 
      int m_CurrentY;      
      int m_ObjectType;
      CFlat* m_pFlat;



};

#endif


