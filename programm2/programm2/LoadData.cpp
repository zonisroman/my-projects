
#include "stdafx.h"
#include "LoadData.h"
#include <conio.h>
#include <ctype.h>
#include <cstdlib>
#include <string>
#include <fstream>
#include <iostream>

using namespace std;

enum EObjType 
{
   eBmp, 
   eTank
};

CLoadData::CLoadData(char* FileName)
{
   if (FileName == NULL)
   {
      return;
   }
   
   m_pFileStream = new ifstream;
   m_pFileStream->open(FileName);
    
   if (m_pFileStream->is_open())
   {
      cout << "\nFile '" << FileName <<"' opened";
      reedData();
      if (m_pFlatList.size() == 0)
      {
         cout << "\n--- Error reading data ---" << endl;
      }
      else
      {
         cout << "\n--- Reading data is completed ---" << endl;
      }
   }
   else 
   {	
      cout << "\nFile '" << FileName <<"' is not opened";
      m_pFileStream = NULL;
   }
}

CLoadData::~CLoadData()
{
   m_pFileStream->close();
   delete m_pFileStream;
   for (int i = 0; i < m_pFlatList.size(); i++)
   {
      delete m_pFlatList[i];
   }

   for (int i = 0; i < m_pObjectList.size() ; i++)
   {
      delete m_pObjectList[i];
   }
}

bool CLoadData::isFileStrmOpen()
{
   if (m_pFileStream == NULL)
   {
      return false;
   }
   return true;
}

void CLoadData::ignoreEmptySymbol()
{
   while ((m_pFileStream->peek() == ' ') || (m_pFileStream->peek() == '\n'))
   {
      m_pFileStream->get();
   }
}

void CLoadData::reedData()
{	
   int CoordX;
   int CoordY;
   string StrType;
   string StrName;
   string StrCoord;
  
   CObjectData* pNewObjectData = NULL;
   CFlatData* pNewFlatData = NULL;

   //Get data from file, create Flat data array, create Object data array
   while (m_pFileStream->peek()!=EOF)
   {
      //get type from file and check it
      getline(*m_pFileStream, StrType,' ');
      cout << "\nType - " << StrType;
      if ((StrType!="Flat") && (StrType!="Tank") && (StrType!="Bmp"))
         {
            cout << "LoadData::reedData:Type is corrupted" ;
            return; 
         }
      ignoreEmptySymbol();
      
      //get name from file
      getline(*m_pFileStream, StrName, ' ');
      cout << "\nName - " << StrName;
      if (StrName.length() == 0)
      {
         cout << "LoadData::reedData: Name is corrupted" ;
         return; 
      }	
      ignoreEmptySymbol();
      
      //get coordinate X from file
      getline(*m_pFileStream, StrCoord, ' ');
      for (int i = 0; i < StrCoord.length(); i++)
      {
         if (!isdigit(StrCoord[i]))
         {
            cout << " LoadData::reedData:" << StrName << " first coordinate error";
            return;
         }
      }
      CoordX = stoi(StrCoord,nullptr,10);
      ignoreEmptySymbol();
      
      //get coordinate Y from file
      getline(*m_pFileStream, StrCoord, '\n');
      for (int i = 0; i < StrCoord.length(); i++)
      {
         if (!isdigit(StrCoord[i]))
         {
            cout << " LoadData::reedData:" << StrName << " second coordinate error";
            return;
         }
      }
      CoordY = stoi(StrCoord, nullptr, 10);
      ignoreEmptySymbol();
      
      //create Data
      if (StrType == "Flat")
      {
         pNewFlatData = new CFlatData(StrName, CoordX, CoordY);
         m_pFlatList.push_back(pNewFlatData);
         cout << "\nReed "<< StrName << " data";
      }
      if ((StrType == "Bmp") || (StrType == "Tank"))
      {
         if (StrType == "Bmp")
         {
            pNewObjectData = new CObjectData(pNewFlatData, eBmp, StrName, CoordX, CoordY);
         }
         if (StrType == "Tank")
         {
            pNewObjectData = new CObjectData(pNewFlatData, eTank, StrName, CoordX, CoordY);
         }	
         m_pObjectList.push_back(pNewObjectData);
         cout << "\nReed object data. Create "	<< 	StrName << " data";	
      }
   }
   return;
}