
#ifndef FLAT_H
#define FLAT_H

#include "MovingObjectList.h"
#include <windows.h>
#include <string>


using namespace std;

class CMovingObject;

class CFlat
{
   public:
      CFlat(string FlatName,int SizeX, int Size); 
      ~CFlat(); 
      int checkPosition(int NewX, int NewY);
      bool addMovingObject(CMovingObject* MovObj);
      bool removeMovingObject(CMovingObject* MovObj);
      CMovingObject* searchByIndex(int Index);
      int getObjectsCount();
      string getFlatName();
      int getSizeX();
      int getSizeY();
      int getMovingObjectLsSize();
      void drawFlat(int ShiftX, int ShiftY);
      void drawMovingObject(CMovingObject* MovObj, int NewX, int NewY);
      COORD m_Shift;
   private:
      void redrawCell(int x, int y);
      string m_FlatName;	
      int m_SizeX;
      int m_SizeY;
      CMovingObjectList m_MovObjLst;
      int m_MovObjLstSize;
};

#endif