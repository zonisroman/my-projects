
#include "stdafx.h"
#include "Flat.h"
#include "MovingObject.h"
#include "MovingObjectList.h"
#include <iostream>
#include <string>

using namespace std;

enum EObjType 
{
   eBmp, 
   eTank
};

CFlat::CFlat(string FlatName, int SizeX, int SizeY) 
{
   m_SizeX = SizeX;
   m_SizeY = SizeY;
   m_FlatName = FlatName;
   m_MovObjLstSize = 0;
   m_Shift.X = 0;
   m_Shift.Y = 0;
}

CFlat::~CFlat()
{
   CMovingObject* MovObj; 
   while (m_MovObjLstSize>0)
   {
      MovObj = m_MovObjLst.searchByIndex(0);
      MovObj->setFlat(NULL);
      m_MovObjLstSize = m_MovObjLst.removeObject(MovObj);
   }
	cout << "\n" << this->m_FlatName <<" is deleted";
}

void CFlat::redrawCell(int x, int y)
{
   COORD cursor;
   cursor.X = m_Shift.X + x;
   cursor.Y = m_Shift.Y + y;
   HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
   SetConsoleTextAttribute(hConsole,0x000C);
   SetConsoleCursorPosition(hConsole,cursor);
   cout << "#";
   Sleep(10);
   SetConsoleTextAttribute(hConsole,0x000F);
   SetConsoleCursorPosition(hConsole,cursor);
   cout << "#";
}

// Checks the coordinates, returns 0 - if coordinates are outside the area, -1 - if coordinates are busy, 1 - if coordinates are free
int CFlat::checkPosition(int NewX, int NewY) 
{
   if ((NewX > m_SizeX) || (NewX < 1) || (NewY > m_SizeY) || (NewY < 1)) 
   {
      redrawCell(NewX, NewY);
      return 0;
   }
   for (int i = 0; i < m_MovObjLstSize; i++)
   {
      if (m_MovObjLstSize == 0)
      {
         return 1;
      }
      if ((NewX == m_MovObjLst.searchByIndex(i)->getCoordX()) && 
         (NewY == m_MovObjLst.searchByIndex(i)->getCoordY()))
      {
         return -1;
      }
   }
   
   return 1;
}

// Adds a pointer to an object in the list
bool CFlat::addMovingObject(CMovingObject *MovObj)
{
   if (MovObj == NULL)
   {
      cout << "\nError adding MovingObject";
      return false;
   }
   m_MovObjLstSize = m_MovObjLst.addObject(MovObj);
   return true;
}	
   

//  Deletes a pointer to an object from the list
bool CFlat::removeMovingObject(CMovingObject *MovObj)
{

   if (MovObj == NULL)
   {
      cout << "\nError removingMovingObject ";
      return false;
   }
   m_MovObjLstSize = m_MovObjLst.removeObject(MovObj);
   return true;
}		

CMovingObject* CFlat::searchByIndex(int Index)
{
   return m_MovObjLst.searchByIndex(Index);
}

int CFlat::getObjectsCount()
{
   return m_MovObjLstSize;
}


//Returns lat name
string CFlat::getFlatName()
{
   return m_FlatName;
}

//Returns area size X
int CFlat::getSizeX()
{
   return m_SizeX;
}

//Returns area size Y		
int CFlat::getSizeY()
{
   return m_SizeY;
}

int CFlat::getMovingObjectLsSize()
{
   return m_MovObjLstSize;
}

void CFlat::drawFlat(int ShiftX, int ShiftY)
{
   m_Shift.X = ShiftX;
   m_Shift.Y = ShiftY;
   HANDLE HConsole = GetStdHandle(STD_OUTPUT_HANDLE);
   SetConsoleCursorPosition(HConsole, m_Shift);
   for (int i = 0; i < (m_SizeY+2); i++)
   {
      for (int j = 0; j < (m_SizeX+2); j++) 
      {
         if ((i == 0) || (i == m_SizeY+1)) 
         {
            cout << "#";
         }
         if ((i > 0) && (i < m_SizeY+1))
         {
            if ((j == 0) || (j == m_SizeX+1))
            {
               cout << "#";
            }
            else
            {
               cout << " ";
            }
         }
      }
      m_Shift.Y++;
      SetConsoleCursorPosition(HConsole, m_Shift);
   }
   m_Shift.Y = m_Shift.Y - (m_SizeY+2);
}


void CFlat::drawMovingObject(CMovingObject *MovObj, int NewX, int NewY)
{
   if (MovObj == NULL)
   {
      return;
   }
   HANDLE HConsole = GetStdHandle(STD_OUTPUT_HANDLE);
   COORD Cursor;
   Cursor.X=m_Shift.X + MovObj->getCoordX();
   Cursor.Y=m_Shift.Y + MovObj->getCoordY();
   SetConsoleCursorPosition(HConsole, Cursor);
   cout << " ";
   Cursor.X=m_Shift.X + NewX;
   Cursor.Y=m_Shift.Y + NewY;	
   SetConsoleCursorPosition(HConsole, Cursor);
   if (MovObj->getType() == eBmp)
   {
      cout << "B";
   }
   if (MovObj->getType() == eTank)
   {
      cout << "T";
   }
}