// programm2.cpp : Defines the entry point for the console application.

#include "stdafx.h"
#include "flat.h"
#include "MovingObjectList.h"
#include "MovingObject.h"
#include "Tank.h"
#include "Bmp.h"
#include "LoadData.h"
#include "FlatData.h"
#include "ObjectData.h"
#include <time.h>
#include <windows.h>
#include <conio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>


using namespace std;

enum EObjType 
{
   eBmp, 
   eTank
};

//Prototype of moveObject function
void moveObject(CMovingObject* MovObj);

//Prototype of createMovingObject function
CMovingObject* createMovingObject(int CoordX, int CoordY, string ObjName, int Objtype, CFlat* pFlat); 

int main(int argc, char* argv[])
{
   //initialize randomizer
   srand(time(NULL));
 
   // Process the input arguments and create object of CLoadData
   // The CLoadData member m_pFlatlist contains Flatlist data 
   // The CLoadData member m_pObjectlist contains MovingObject data 
   
   CLoadData* OpenedFile;
   if (argc != 2)
   {
      cout << "\nArguments are not defined correctly";
      cout << "\nSyntax: programm2 data_file";
      return -1;
   }
   else
   {
      OpenedFile = new CLoadData(argv[1]);
      if (OpenedFile->isFileStrmOpen() == false)
      {
         return -1;
      }
      if ((OpenedFile->m_pFlatList.size() == NULL) || (OpenedFile->m_pObjectList.size()  == NULL)) 
      {
         return -1;
      }
   }		
   
   // Get Flat parameters from OpenedFile, create Flat and put Flat pointer to the GlobalFlatList
   CMovingObject* pNewMovObj;
   CFlat* pNewFlat;
   vector <CFlat*> globalFlatList;
   CMovingObjectList globaMovObjList;
   int globaMovObjListSize = 0;
   
   for (int i = 0; i < OpenedFile->m_pFlatList.size(); i++)
   {
      if ((OpenedFile->m_pFlatList[i]->m_FlatSizeX <= 1) || (OpenedFile->m_pFlatList[i]->m_FlatSizeY <= 1))   
      {
         cout << "\n" << OpenedFile->m_pFlatList[i]->m_FlatName <<" size is incoorect";
         continue;
      }
      else
      {
         pNewFlat = new CFlat (OpenedFile->m_pFlatList[i]->m_FlatName, 
            OpenedFile->m_pFlatList[i]->m_FlatSizeX, 
            OpenedFile->m_pFlatList[i]->m_FlatSizeY);
         if (pNewFlat != NULL)
         {
            globalFlatList.push_back(pNewFlat);
         //   cout << "\n" << pFlatList[FlatListSize-1]->getFlatName() <<" created succesfully. Area size " <<pFlatList[FlatListSize-1]->getSizeX() << "x" << pFlatList[FlatListSize-1]->getSizeY() << ".";
         }
      }
      
      // Get MovingObject parameters from OpenedFile, create MovingObject
      // and put MovingObject pointer to the GlobalMovingObjectList 
      for (int j = 0; j < OpenedFile->m_pObjectList.size(); j++)
      {
         if (OpenedFile->m_pObjectList[j]->m_pFlat == OpenedFile->m_pFlatList[i])
         {
            pNewMovObj = createMovingObject(OpenedFile->m_pObjectList[j]->m_ObjCoordX, 
               OpenedFile->m_pObjectList[j]->m_ObjCoordY, 
               OpenedFile->m_pObjectList[j]->m_ObjName, 
               OpenedFile->m_pObjectList[j]->m_ObjType, 
               globalFlatList[i]);
         
            if (pNewMovObj == NULL) 
            {
               cout << "\n" <<  OpenedFile->m_pObjectList[j]->m_ObjName << " is not created";
            }
            else 
            {
               globaMovObjListSize = globaMovObjList.addObject(pNewMovObj);
               cout << "\nCreate an object " << pNewMovObj->getName() 
                  << " at a point (" << pNewMovObj->getCoordX() 
                  << "," << pNewMovObj->getCoordY() << ")";
            }						
         }
      }
   }
   
   //Delet source data
   delete OpenedFile;
   OpenedFile=NULL;
   
    cout << "\n---Creating flats and objects is completed ---" << endl << endl;
   
   //Draws flats and Movingobjects	
   HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
   SetConsoleTextAttribute(hConsole, 0x000F);
   CONSOLE_SCREEN_BUFFER_INFO bufInfo;
   GetConsoleScreenBufferInfo(hConsole, &bufInfo);
   int shiftY = bufInfo.dwCursorPosition.Y;

   for (int i = 0; i < globalFlatList.size(); i++)
   {
      GetConsoleScreenBufferInfo(hConsole, &bufInfo);
      globalFlatList[i]->drawFlat(1, shiftY);
      for (int j = 0; j < globalFlatList[i]->getMovingObjectLsSize(); j++)
      {
         globalFlatList[i]->drawMovingObject(globalFlatList[i]->searchByIndex(j), 
            globalFlatList[i]->searchByIndex(j)->getCoordX(), 
            globalFlatList[i]->searchByIndex(j)->getCoordY());
      }
      shiftY = shiftY + globalFlatList[i]->getSizeY() + 2;
   }
   
   // Moves MovingObject
   while(!_kbhit()) 
   {
      for (int i = 0; i < globalFlatList.size(); i++)
      {
         for (int j = 0; j < globalFlatList[i]->getObjectsCount(); j++)
         {
            moveObject(globalFlatList[i]->searchByIndex(j));
            Sleep(10);
         }
      }
   }
   
   // Set cursor in console window
   COORD cursor;
   cursor.X = 0;
   cursor.Y = shiftY;
   SetConsoleCursorPosition(hConsole, cursor);
   
   //Destructs MovingObjects
   CMovingObject* MovObj;
   while (globaMovObjListSize > 0)
   {
      MovObj = globaMovObjList.searchByIndex(0);
      globaMovObjListSize = globaMovObjList.removeObject(MovObj);
      delete MovObj;
   }
   
   //Destructs flats
   vector <CFlat*>::iterator iter;
   while (globalFlatList.size() > 0)
   {
      delete globalFlatList[globalFlatList.size()-1];
      iter = globalFlatList.end();
      globalFlatList.pop_back();
   }
   return 0;
}

// Function creates MovingObject and returns pointer to it or returns NULL
CMovingObject* createMovingObject(int CoordX, int CoordY, string ObjName, int ObjType, CFlat* pFlat)
{
   
   if (ObjName.length() == 0)
   {
      cout << "\nObject name is not specified";
      return NULL;
   }
   if (pFlat == NULL)
   {
      cout << "\nFlat is not specified";
      return NULL;	
   }
   if ((ObjType < 0) || (ObjType > 1))
   {
      cout << "\n" << ObjName << " Object type is incorrect";
      return NULL;	
   }
   if (pFlat->checkPosition(CoordX, CoordY) != 1)
   {
      cout << "\n" << ObjName << " coordinates are incorrect";
      return NULL;		
   }
   
   CMovingObject* MovObj = NULL;
   switch (ObjType)
   {
      case eBmp:
         MovObj = new CBmp(CoordX, CoordY, ObjName, ObjType, pFlat);
         break;
      case eTank:
         MovObj = new CTank(CoordX, CoordY, ObjName, ObjType, pFlat);
         break;
   }
   
   return MovObj;
   
}

//Function generates MovingObject movement
void moveObject(CMovingObject* MovObj)
{
   switch (rand()%4)
      {
         case 0:
            MovObj->moveX(1);
            break;
         case 1:
            MovObj->moveY(1);
            break;
         case 2:
            MovObj->moveX(-1);
            break;
         case 3:
            MovObj->moveY(-1);
            break;
      }
}