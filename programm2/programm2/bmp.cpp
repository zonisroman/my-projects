
#include "stdafx.h"
#include "Flat.h"
#include "Bmp.h"
#include <iostream>
#include <string>

using namespace std;

CBmp::CBmp(int x,  int y, string ObjName, int ObjType, CFlat* pFlat):CMovingObject(x,y, ObjName, ObjType, pFlat)
{
   m_pFlat->addMovingObject(this);

//	cout << "\nCreating BMP";
}

CBmp::~CBmp()
{
   if (m_pFlat != NULL)
   {
      m_pFlat->removeMovingObject(this);
   }
   
   cout << "\n" << m_ObjectName << " is deleted";

}

void CBmp::moveX(int ShiftX)
{
   if (m_pFlat == NULL)
   {
      return;
   }
   switch (m_pFlat->checkPosition(m_CurrentX+ShiftX, m_CurrentY))
   {
      case 1:
         m_pFlat->drawMovingObject(this, m_CurrentX + ShiftX, m_CurrentY);
         m_CurrentX = m_CurrentX+ShiftX;
      //	cout << "\n" << getName() << " moved to the point ("  << m_currentX << "," << m_currentY<< ")";
         break;
      case -1:
      //	cout << "\n" << getName() << " tried to run over another object ("  << m_currentX+shiftX << "," << m_currentY<< ")";
         break;
      case 0:
      //	cout << "\n" << getName() << " tried to leave the area ("  << m_currentX+shiftX << "," << m_currentY<< ")";
         break;
   }
}

void CBmp::moveY(int ShiftY)
{
   if (m_pFlat == NULL)
   {
      return;
   }
   switch (m_pFlat->checkPosition(m_CurrentX, m_CurrentY + ShiftY))
   {
      case 1:
         m_pFlat->drawMovingObject(this, m_CurrentX, m_CurrentY + ShiftY);
         m_CurrentY = m_CurrentY + ShiftY;
      //	cout << "\n" << getName() << " moved to the point ("  << m_currentX << "," << m_currentY<< ")";
         break;
      case -1:
      //	cout << "\n" << getName() << " tried to run over another object ("  << m_currentX << "," << m_currentY+shiftY<< ")";
         break;
      case 0:
      //	cout << "\n" << getName() << " tried to leave the area ("  << m_currentX << "," << m_currentY+shiftY << ")";
         break;
   }
}

string CBmp::getName()
{
   return m_ObjectName;
}

void CBmp::setFlat(CFlat* pFlat)
{

   m_pFlat = pFlat;
   if (pFlat == NULL)
   {
      m_CurrentX = 0;
      m_CurrentY = 0;
   }
   else
   {
      do
      {
         m_CurrentX = (rand()%m_pFlat->getSizeX()) + 1;
         m_CurrentY = (rand()%m_pFlat->getSizeY()) + 1;
      }
      while (m_pFlat->checkPosition(m_CurrentX, m_CurrentY) != 1);
      m_pFlat->addMovingObject(this);
   }
}
