#ifndef FLAT_DATA_H
#define FLAT_DATA_H

#include <string>

using namespace std;

class CFlatData
{		
   public:
      CFlatData(string FlatName, int SizeX, int SizeY);
      ~CFlatData();
      string m_FlatName;
      int m_FlatSizeX;
      int m_FlatSizeY;
};

#endif