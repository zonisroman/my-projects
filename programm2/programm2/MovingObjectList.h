
#ifndef MOVING_OBJECT_LIST_H
#define MOVING_OBJECT_LIST_H
#include <vector>

class CMovingObject;

class CMovingObjectList
{
   public:
      CMovingObjectList();
      ~CMovingObjectList();
      int addObject(CMovingObject* MovObj);
      int removeObject(CMovingObject* MovObj);
      CMovingObject* searchByIndex(int Index);
   private:
      std::vector <CMovingObject*> m_pMovingObjectArray;	

};

#endif