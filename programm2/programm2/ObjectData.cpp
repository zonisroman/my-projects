
#include "stdafx.h"
#include "ObjectData.h"
#include <iostream>
#include <string>


using namespace std;

CObjectData::CObjectData(CFlatData* pFlat, int ObjType, string ObjName, int x, int y)
{
   if (pFlat == NULL)
   {
      return;
   }
   m_pFlat = pFlat;
   m_ObjType = ObjType;
   m_ObjCoordX = x;
   m_ObjCoordY = y;
   m_ObjName = ObjName;
//	cout << "\nObject " << m_ObjName << "data is created";
}
CObjectData::~CObjectData()
{
   //	cout << "\nObject" << m_ObjName << " data is deleted";
}