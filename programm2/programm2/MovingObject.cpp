
#include "stdafx.h"
#include "MovingObject.h"
#include "Flat.h"
#include <string>
//#include <iostream>

using namespace std;

CMovingObject::CMovingObject(int x, int y, string ObjectName, int ObjectType, CFlat* pFlat) 
{
   m_pFlat = pFlat;
   m_CurrentX = x;
   m_CurrentY = y;
   m_ObjectName = ObjectName;
   m_ObjectType = ObjectType;
   //	cout << "\nCreating MovingObject";
}

CMovingObject::~CMovingObject() 
{
//	cout << "\nDeleting MovingObject";
}

void CMovingObject::moveX(int ShiftX)
{
   if (m_pFlat == NULL)
   {
      return;
   }
   switch (m_pFlat->checkPosition(m_CurrentX + ShiftX, m_CurrentY))
   {
      case 1:
         m_pFlat->drawMovingObject(this, m_CurrentX + ShiftX, m_CurrentY);
         m_CurrentX = m_CurrentX + ShiftX;
      //	cout << "\n" << getName() << " moved to the point ("  << m_currentX << "," << m_currentY<< ")";
         break;
      case -1:
      //	cout << "\n" << getName() << " tried to run over another object ("  << m_currentX+shiftX << "," << m_currentY<< ")";
         break;
      case 0:
      //	cout << "\n" << getName() << " tried to leave the area ("  << m_currentX+shiftX << "," << m_currentY<< ")";
         break;
   }
}

void CMovingObject::moveY(int ShiftY) 
{
   if (m_pFlat==NULL)
   {
      return;
   }
   switch (m_pFlat->checkPosition(m_CurrentX, m_CurrentY + ShiftY))
   {
      case 1:
         m_pFlat->drawMovingObject(this, m_CurrentX, m_CurrentY + ShiftY);
         m_CurrentY=m_CurrentY+ShiftY;
      //	cout << "\n" << getName() << " moved to the point ("  << m_currentX << "," << m_currentY<< ")";
         break;
      case -1:
      //	cout << "\n" << getName() << " tried to run over another object ("  << m_currentX << "," << m_currentY+shiftY<< ")";
         break;
      case 0:
      //	cout << "\n" << getName() << " tried to leave the area ("  << m_currentX << "," << m_currentY+shiftY << ")";
         break;
   }
}

int CMovingObject::getCoordX()
{
   return m_CurrentX;
}

int CMovingObject::getCoordY()
{
   return m_CurrentY;
}

string CMovingObject::getName()
{
   return m_ObjectName;
}

CFlat* CMovingObject::getFlat()
{
   return m_pFlat;
}

void CMovingObject::setFlat(CFlat* pFlat)
{
   m_pFlat=pFlat;
}

int CMovingObject::getType()
{
   return m_ObjectType;
}