
#ifndef TANK_H
#define TANK_H

#include "MovingObject.h"
#include <string>

using namespace std;

class CTank: public CMovingObject
{
   public:
      CTank(int x,  int y, string ObjName, int ObjType, CFlat* pFlat);
      virtual ~CTank();
      virtual void moveX (int ShiftX);
      virtual void moveY (int ShiftY);
      virtual string getName();
      virtual void setFlat(CFlat* pFlat);
};

#endif

