
#include "stdafx.h"
#include "MovingObjectList.h"
#include "MovingObject.h"
#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

CMovingObjectList::CMovingObjectList()
{

}

CMovingObjectList::~CMovingObjectList()
{

}

int CMovingObjectList::addObject(CMovingObject* MovObj)
{
   if (MovObj == NULL)
   {
      cout << "\nError adding MovingObject";
      return m_pMovingObjectArray.size();
   }
   m_pMovingObjectArray.push_back(MovObj);
   return m_pMovingObjectArray.size();
}

int CMovingObjectList::removeObject(CMovingObject* MovObj)
{

   if (MovObj == NULL)
   {
      cout << "\nError removing MovingObject";
      return m_pMovingObjectArray.size();
   }
   for (int i = 0; i < m_pMovingObjectArray.size(); i++)
   {
      if (m_pMovingObjectArray[i] == MovObj) 
      {	
         m_pMovingObjectArray.erase(m_pMovingObjectArray.begin()+i);
         return m_pMovingObjectArray.size();
      }
   }
   return m_pMovingObjectArray.size();
}	

CMovingObject* CMovingObjectList::searchByIndex(int Index)
{
   if (Index > m_pMovingObjectArray.size()-1)
   {
      return NULL;
   }
   return m_pMovingObjectArray[Index];
}