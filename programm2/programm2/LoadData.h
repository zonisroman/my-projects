
#ifndef LOAD_DATA_H
#define LOAD_DATA_H

#include "FlatData.h"
#include "ObjectData.h"
#include <fstream>
#include <vector>

class CLoadData
{
   public:
      CLoadData(char* FileName);
      ~CLoadData();
      bool isFileStrmOpen();
      vector <CFlatData*> m_pFlatList;
      vector <CObjectData*> m_pObjectList;
   private:
        ifstream* m_pFileStream;
        void ignoreEmptySymbol();
        void reedData();
};

#endif