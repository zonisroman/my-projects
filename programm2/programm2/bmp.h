
#ifndef BMP_H
#define BMP_H

#include "MovingObject.h"
#include <string>

using namespace std;

class CBmp: public CMovingObject
{
      
   public:
      CBmp(int x,  int y, string ObjName, int ObjType, CFlat* pFlat);
      virtual ~CBmp();
      virtual void moveX (int ShiftX);
      virtual void moveY (int ShiftY);
      virtual string getName();
      virtual void setFlat(CFlat* pFlat);
};

#endif