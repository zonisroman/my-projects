
#pragma once

#include "stdafx.h"

// Declaration of an frame-window class

class CProgrammMainFrame: public CFrameWnd
{
   public:
      CProgrammMainFrame();
      ~CProgrammMainFrame();
      BOOL PreCreateWindow(CREATESTRUCT& cs);
      afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
      CMenu mainMenu;
      DECLARE_MESSAGE_MAP();
      
    
};