
#pragma once

#include "stdafx.h"

// Declaration of an application class
class CProgrammApp: public CWinApp
{
   public:
      CProgrammApp();
      ~CProgrammApp();
      BOOL InitInstance();
};