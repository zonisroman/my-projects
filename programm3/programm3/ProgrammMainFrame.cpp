
#include "stdafx.h"
#include "Resource.h"
#include "ProgrammMainFrame.h"


BEGIN_MESSAGE_MAP (CProgrammMainFrame, CFrameWnd)
   ON_WM_LBUTTONDOWN()
   
END_MESSAGE_MAP()

CProgrammMainFrame::CProgrammMainFrame()
{
  
  
   Create(NULL, "MovingObjects MFC Application");
   if (m_hMenu==NULL)
   {
      mainMenu.LoadMenu(IDR_MAINFRAME);
      SetMenu(&mainMenu);
    
   } 

}

CProgrammMainFrame::~CProgrammMainFrame()
{
   
}

BOOL CProgrammMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
 
   cs.x = 700;
   cs.y = 200;
   cs.cx = 500;
   cs.cy = 500;
   cs.style = WS_OVERLAPPEDWINDOW;
   
   return CFrameWnd::PreCreateWindow(cs);
}

void CProgrammMainFrame::OnLButtonDown(UINT nFlags, CPoint point)
{
   CClientDC clientDC(this);
   clientDC.TextOut(point.x, point.y, "Click");
}



