//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by programm3.rc
//
#define IDC_MYICON                      2
#define IDC_PROGRAMM3                   3
#define IDS_APP_TITLE                   103
#define IDI_ICON1                       137
#define IDR_MAINFRAME                   138
#define ID_FILE_OPEN                    32777
#define ID_FILE_EXIT                    32778
#define ID_HELP_ABOUT                   32779
#define ID_START                        32780

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        139
#define _APS_NEXT_COMMAND_VALUE         32781
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
