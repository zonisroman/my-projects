
#include "stdafx.h"
#include "ProgrammApp.h"
#include "ProgrammMainFrame.h"


CProgrammApp::CProgrammApp()
{
  
}

CProgrammApp::~CProgrammApp()
{
}

BOOL CProgrammApp::InitInstance()
{
   
   m_pMainWnd = new CProgrammMainFrame();
   m_pMainWnd->ShowWindow(m_nCmdShow);
       
   return TRUE;
}

CProgrammApp app;